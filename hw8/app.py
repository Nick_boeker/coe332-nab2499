from flask import Flask, jsonify, request
from hw1 import csv_to_dict, time_call, lim_and_off
import json
import jobs

app = Flask(__name__)

@app.route('/rainfall', methods=['GET'])
def get_all_rainfall():
    start = request.args.get('start')
    end = request.args.get('end')
    limit = request.args.get('limit')
    offset = request.args.get('offset')

    if start is not None:
        try:
            start = int(start)
        except:
            return jsonify("Please enter a valid start year"),400
    
    if end is not None:
        try:
            end = int(end)
        except: 
            return jsonify("Please enter a valid end year"),400

    if limit is not None:
        try:    
            limit = int(limit)
        except:
            return jsonify("Please enter a valid limit"),400

    if offset is not None:
        try:
            offset = int(offset)
        except:
            return jsonify("Please enter a valid offset"),400

    if (start is not None or end is not None) and (limit is not None or offset is not None):
        return jsonify("Please select start/end or limit/offset, not both"),400

    data = csv_to_dict()

    if start is not None or end is not None:
        if start is None:
            return jsonify(time_call(data,end=end))
        elif end is None:
            return jsonify(time_call(data,start=start))
        else:
            return jsonify(time_call(data,start=start,end=end))

    if limit is not None or offset is not None:
        if limit is None:
            return jsonify(lim_and_off(data,offset=offset))
        elif offset is None:
            return jsonify(lim_and_off(data,limit=limit))
        else:
            return jsonify(lim_and_off(data,limit=limit,offset=offset))

    return jsonify(data)

@app.route('/rainfall/<entry_id>',methods=['GET'])
def get_by_id(entry_id):
    try:
        entry_id=int(entry_id)
    except:
        return jsonify("Please enter a valid row id"),400
    if entry_id < 0:
        return jsonify("Please enter a valid row id"),400
    return jsonify(csv_to_dict()[entry_id])

@app.route('/rainfall/year/<year>',methods=['GET'])
def get_by_year(year):
    try:
        year = int(year)
    except:
        return jsonify("Please enter a valid year"),400
    if year < 0:
        return jsonify("Please enter a valid year"),400
    return jsonify(time_call(csv_to_dict(),start=year,end=year)[0])

@app.route('/jobs',methods=['GET','POST'])
def jobs_api():
    if request.method == 'POST':
        data = request.get_json(force=True)

        start = data.get('start')
        end = data.get('end')
        limit = data.get('limit')
        offset = data.get('offset')

        if start:
            try:
                start=int(start)
            except:
                return "Error converting start date"
            
        if end:
            try:
                end=int(end)
            except:
                return "Error converting end date"
        
        if limit:
            try:
                limit=int(limit)
            except:
                return "Error converting limit value"

        if offset:
            try:
                offset=int(offset)
            except:
                return "Error converting offset value"

        if (start or end) and (limit or offset):
            return "Please only use start/end or limit/offset, not a combination"

        fyear=1850
        lyear=1979
        if start:
            if end:
                jobs.add_job(start,end)
            else:
                jobs.add_job(start,lyear)
        elif end:
            jobs.add_job(fyear,end)
        elif limit:
            if offset:
                jobs.add_job(fyear+offset,fyear+offset+limit)
            else:
                jobs.add_job(fyear,fyear+limit)
        elif offset:
            jobs.add_job(fyear+offset,lyear)
        
        return "Job Submitted"

    elif request.method == 'GET':
        return json.dumps(jobs.get_all_jobs())

@app.route('/jobs/<jid>',methods=['GET'])
def get_job_by_jid(jid):
    print(jobs.get_job(jid))
    return json.dumps(jobs.get_job(jid))

if __name__=="__main__":
    app.run(debug=True, host='0.0.0.0')
