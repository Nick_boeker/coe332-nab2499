#from hotqueue import HotQueue
import redis
import json
import uuid
import datetime

def generate_jid():
    return str(uuid.uuid4())

def generate_job_key(jid):
    return 'job.{}'.format(jid)

def instantiate_job(jid,status,start,end):
    if type(jid)==str:
        return {'id':jid,
                'status': status,
                'start': start,
                'end': end,
                'create':str(datetime.datetime.now()),
                'updated':str(datetime.datetime.now())
                }
    return {'id': jid.decode('utf-8'),
            'status': status.decode('utf-8'),
            'start': start.decode('utf-8'),
            'end': end.decode('utf-8'),
            'create': str(datetime.datetime.now().decode('utf-8')),
            'updated': str(datetime.datetime.now().decode('utf-8'))
            }

def save_job(job_key, job_dict):
        rd.set(job_key,job_dict)

#def queue_job(jid):
#    q.put(jid)

def add_job(start,end,status="submitted"):
    """Add a job to the redis queue."""
    jid = generate_jid()
    job_dict = instantiate_job(jid,status,start,end)
    save_job(generate_job_key(jid),job_dict)
#    queue_job(jid)

def update_job_status(jid, status):
    jid,status,start,end = rd.get(generate_job_key(jid),'id','status','start','end')
    job = _instantiate_job(jid,status,start,end)
    if job:
        job['status'] = status
        _save_job(_generate_job_key(jid),job)
    else:
        raise Exception()

def get_all_jobs():
    jobs=[]
    for key in rd.keys():
        jobs.append(eval(rd.get(key.decode('utf-8')).decode('utf-8')))
    return jobs

def get_job(jid):
    job_key=generate_job_key(jid)
    if rd.get(job_key):
        return rd.get(job_key).decode('utf-8')
    else:
        return "Job not found",400

#q = HotQueue("queue",host='localhost',port=6379,db=1)
rd = redis.StrictRedis(host='172.17.0.1',port=6379,db=0)
