import requests

def test_root_status():
    rsp = requests.get("http://localhost:5000/rainfall")
    assert rsp.status_code==200

def test_root_json():
    rsp = requests.get("http://localhost:5000/rainfall")
    assert rsp.json()
    
def test_root_json_type():
    rsp = requests.get("http://localhost:5000/rainfall")
    assert type(rsp.json())==list
    
def test_root_list_item_types():
    rsp = requests.get("http://localhost:5000/rainfall")
    for item in rsp.json():
        assert type(item) == dict

def test_root_dict_key_count():
    rsp = requests.get("http://localhost:5000/rainfall")
    for item in rsp.json():
        assert len(item.keys())==3

def test_root_dict_key_type():
    rsp = requests.get("http://localhost:5000/rainfall")
    for item in rsp.json():
        for key in item.keys():
            assert type(key)==str

def test_root_list_len():
    rsp = requests.get("http://localhost:5000/rainfall")
    assert len(rsp.json())==130

def test_root_values():
    rsp = requests.get("http://localhost:5000/rainfall")
    data = rsp.json()
    assert data[0]['id']==0
    assert data[0]['year']==1850
    assert data[0]['rain']==852

    assert data[-1]['id']==129
    assert data[-1]['year']==1979
    assert data[-1]['rain']==996

def test_start_ls_end():
    rsp = requests.get("http://localhost:5000/rainfall?start=1900&end=1950")
    assert rsp.status_code==200
    assert rsp.json()
    assert type(rsp.json())==list
    assert len(rsp.json())==51

def test_start_only():
    rsp = requests.get("http://localhost:5000/rainfall?start=1970")
    assert rsp.status_code==200
    assert rsp.json()
    assert type(rsp.json())==list
    assert len(rsp.json())==10

def test_end_only():
    rsp = requests.get("http://localhost:5000/rainfall?end=1970")
    assert rsp.status_code==200
    assert rsp.json()
    assert type(rsp.json())==list
    assert len(rsp.json())==121

def test_start_gt_end():
    rsp = requests.get("http://localhost:5000/rainfall?start=1971&end=1970")
    assert rsp.status_code==200
    assert rsp.json()==[]
    assert type(rsp.json())==list
    assert len(rsp.json())==0

def test_lim_and_off():
    rsp = requests.get("http://localhost:5000/rainfall?limit=5&offset=3")
    assert rsp.status_code==200
    assert rsp.json()
    assert type(rsp.json())==list
    assert len(rsp.json())==5

def test_lim_only():
    rsp = requests.get("http://localhost:5000/rainfall?limit=70")
    assert rsp.status_code==200
    assert rsp.json()
    assert type(rsp.json())==list
    assert len(rsp.json())==70

def test_off_only():
    rsp = requests.get("http://localhost:5000/rainfall?offset=70")
    assert rsp.status_code==200
    assert rsp.json()
    assert type(rsp.json())==list
    assert len(rsp.json())==60

def test_invalid_start():
    rsp = requests.get("http://localhost:5000/rainfall?start=abc")
    assert rsp.status_code==400
    assert rsp.json()

def test_invalid_end():
    rsp = requests.get("http://localhost:5000/rainfall?end=abc")
    assert rsp.status_code==400
    assert rsp.json()

def test_invalid_lim():
    rsp = requests.get("http://localhost:5000/rainfall?limit=abc")
    assert rsp.status_code==400
    assert rsp.json()

def test_invalid_off():
    rsp = requests.get("http://localhost:5000/rainfall?offset=abc")
    assert rsp.status_code==400
    assert rsp.json()

def test_mixed_input():
    rsp = requests.get("http://localhost:5000/rainfall?start=1900&limit=4")
    assert rsp.status_code==400
    assert rsp.json()

def test_get_id():
    rsp = requests.get("http://localhost:5000/rainfall/7")
    assert rsp.status_code==200
    assert rsp.json()
    assert type(rsp.json())==dict
    assert len(rsp.json().keys())==3
    assert rsp.json()['id']==7

def test_get_year():
    rsp = requests.get("http://localhost:5000/rainfall/year/1970")
    assert rsp.status_code==200
    assert rsp.json()
    assert type(rsp.json())==dict
    assert len(rsp.json().keys())==3
    assert rsp.json()['year']==1970
    assert rsp.json()['id']==120
