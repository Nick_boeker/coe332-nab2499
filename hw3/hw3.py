from flask import Flask, jsonify, request
from hw1 import csv_to_dict, time_call, lim_and_off

app = Flask(__name__)

@app.route('/rainfall', methods=['GET'])
def get_all_rainfall():
    start = request.args.get('start')
    end = request.args.get('end')
    limit = request.args.get('limit')
    offset = request.args.get('offset')

    if start is not None:
        try:
            start = int(start)
        except:
            return "Please enter a valid start year"
    
    if end is not None:
        try:
            end = int(end)
        except: 
            return "Please enter a valid end year"

    if limit is not None:
        try:    
            limit = int(limit)
        except:
            return "Please enter a valid limit"

    if offset is not None:
        try:
            offset = int(offset)
        except:
            return "Please enter a valid offset"

    if (start is not None or end is not None) and (limit is not None or offset is not None):
        return "Please select start/end or limit/offset, not both"

    data = csv_to_dict()

    if start is not None or end is not None:
        if start is None:
            return jsonify(time_call(data,end=end))
        elif end is None:
            return jsonify(time_call(data,start=start))
        else:
            return jsonify(time_call(data,start=start,end=end))

    if limit is not None or offset is not None:
        if limit is None:
            return jsonify(lim_and_off(data,offset=offset))
        elif offset is None:
            return jsonify(lim_and_off(data,limit=limit))
        else:
            return jsonify(lim_and_off(data,limit=limit,offset=offset))

    return jsonify(data)

@app.route('/rainfall/<entry_id>',methods=['GET'])
def get_by_id(entry_id):
    try:
        entry_id=int(entry_id)
    except:
        return "Please enter a valid row id"
    if entry_id < 0:
        return "Please enter a valid row id"
    return jsonify(csv_to_dict()[entry_id])

@app.route('/rainfall/year/<year>',methods=['GET'])
def get_by_year(year):
    try:
        year = int(year)
    except:
        return "Please enter a valid year"
    if year < 0:
        return "Please enter a valid year"
    return jsonify(time_call(csv_to_dict(),start=year,end=year))

if __name__=="__main__":
    app.run(debug=True, host='0.0.0.0')
