from hw1 import csv_to_dict, time_call, lim_and_off

def test_csv2dict_return_list():
  assert type(csv_to_dict())==list
def test_csv2dict_element_dict():
  for item in csv_to_dict():
    assert type(item) == dict
def test_csv2dict_key_count():
  for item in csv_to_dict():
    assert len(item.keys()) == 3
def test_csv2dict_verify_keys():
  for item in csv_to_dict():
    for key in sorted(item.keys()):
      assert type(key) == str
def test_csv2dict_dict_quantity():
  assert len(csv_to_dict()) == 130
def test_csv2dict_verify_dict_values():
  assert csv_to_dict()[0]['id']==0
  assert csv_to_dict()[0]['year']==1850
  assert csv_to_dict()[0]['rain']==852
  assert csv_to_dict()[-1]['id']==129
  assert csv_to_dict()[-1]['year']==1979
  assert csv_to_dict()[-1]['rain']==996.0

def test_timecall_return_list():
    data=csv_to_dict()
    assert type(time_call(data))==list
def test_timecall_element_dict():
    data=csv_to_dict()
    for item in time_call(data):
        assert type(item) == dict
def test_timecall_key_count():
    data=csv_to_dict()
    for item in time_call(data):
        assert len(item.keys())==3
def test_timecall_verify_keys():
    data=csv_to_dict()
    for item in time_call(data):
        for key in item.keys():
            assert type(key) == str
def test_timecall_verify_no_args():
    data=csv_to_dict()
    res=time_call(data)
    assert len(res)==130
    assert res[0]['id']==0
    assert res[-1]['id']==129
def test_timecall_verify_no_end():
    data=csv_to_dict()
    res=time_call(data,start=1860)
    assert len(res)==120
    assert res[0]['id']==10
    assert res[-1]['id']==129
def test_timecall_verify_no_start():
    data=csv_to_dict()
    res=time_call(data,end=1970)
    assert len(res)==121
    assert res[0]['id']==0
    assert res[-1]['id']==120
def test_timecall_verify_both_args():
    data=csv_to_dict()
    res=time_call(data,start=1900,end=1959)
    assert len(res)==60
    assert res[0]['id']==50
    assert res[-1]['id']==109
def test_timecall_start_greater_end():
    data=csv_to_dict()
    res=time_call(data,start=1900,end=1880)
    assert len(res)==0

def test_limoff_return_list():
    data=csv_to_dict()
    assert type(lim_and_off(data))==list
def test_limoff_element_dict():
    data=csv_to_dict()
    for item in lim_and_off(data):
        assert type(item)==dict
def test_limoff_key_count():
    data=csv_to_dict()
    for item in lim_and_off(data):
        assert len(item.keys())==3
def test_limoff_verify_keys():
    data=csv_to_dict()
    for item in lim_and_off(data):
        for key in item.keys():
            assert type(key)==str
def test_limoff_verify_no_args():
    data=csv_to_dict()
    res=lim_and_off(data)
    assert len(res)==130
    assert res[0]['id']==0
    assert res[-1]['id']==129
def test_limoff_verify_no_idx():
    data=csv_to_dict()
    res=lim_and_off(data,limit=26)
    assert len(res)==26
    assert res[0]['id']==0
    assert res[-1]['id']==25
def test_limoff_verify_no_lim():
    data=csv_to_dict()
    res=lim_and_off(data,offset=9)
    assert len(res)==121
    assert res[0]['id']==9
    assert res[-1]['id']==129
def test_limoff_verify_both_args():
    data=csv_to_dict()
    res=lim_and_off(data,limit=10,offset=10)
    assert len(res)==10
    assert res[0]['id']==10
    assert res[-1]['id']==19
def test_limoff_lim_greater_len():
    data=csv_to_dict()
    res=lim_and_off(data,limit=5000)
    assert len(res)==130
    assert res[0]['id']==0
    assert res[-1]['id']==129
