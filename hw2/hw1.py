def csv_to_dict():
  data=[]
  with open("annual-rainfall-fortaleza-brazil.csv") as csvfile:
    i=0
    first=1
    for line in csvfile:
      if first:
        first=0
        continue
      arr = line.split(',')
      tmp = {}
      tmp['id'] = i
      tmp['year'] = int(arr[0].strip('"'))
      tmp['rain'] = float(arr[1].rstrip('\n'))
      data.append(tmp)
      i=i+1
  return data

def time_call(data,start=-9e20,end=9e20):
  pdata = []
  for line in data:
    if ((line['year'] >= start) and (line['year'] <= end)):
      pdata.append(line)
  return pdata

def lim_and_off(data,limit=-1,offset=0):
  pdata = []
  if limit==-1:
    limit = len(data)

  for i in range(limit):
    if i+offset >= len(data):
      break
    else:
      pdata.append(data[i+offset])
  return pdata
      
def main():
  data = csv_to_dict()
  print("Choose which type of query")
  choice = input("a = all data, b= search by year, c=search by limit and offset: ")
  if choice=='a':
    [print(i) for i in data]
  elif choice=='b':
    while 1:
      try:
        start_year = int(input("Please input the year to start with or enter -1 to start at beginning: "))
        break
      except:
        print("Please input a valid start year")
    while 1:
      try:
        stop_year = int(input("Please input the year to end with or enter -1 to end at end of data: "))
        break
      except:
        print("Please input a valid end year")
    if start_year==-1:
      start_year=-9e20
    if stop_year==-1:
      stop_year=9e20
 
    pdata = time_call(data,start=start_year,end=stop_year)
    [print(i) for i in pdata]
  elif choice=='c':
    while 1:
      try:
        limit = int(input("Please enter the number of entries you want or enter -1 for all entries: "))
        break
      except:
        print("Please input a valid amount")
    while 1:
      try:
        offset = int(input("Please enter the offset you would like or enter -1 for no offset: "))
        break
      except:
        print("Please input a valid offset")
    if offset==-1:
      offset=0
    pdata = lim_and_off(data,limit,offset)
    [print(i) for i in print(pdata)]      

  else:
    print("Please input a valid choice")

if __name__ == "__main__":
  main()
